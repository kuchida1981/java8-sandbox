package org.bitbucket.kuchida1981.sandbox;

import static org.junit.Assert.*;

import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.bitbucket.kuchida1981.sandbox.samples.Person;
import org.bitbucket.kuchida1981.sandbox.samples.Sex;
import org.junit.Test;

/**
 * Stream API (とラムダ) のサンプル集
 * 
 * @author kosuke
 *
 */
public class StreamExamples {

	/**
	 * 100までの素数一覧
	 */
	final private int[] PRIMES100 = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31,
			37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, };

	/**
	 * 北海道の市の一覧
	 */
	final private String[] HOKKAIDO_CITIES = { "10 岩見沢", "31 恵庭", "2 函館",
			"29 富良野", "21 名寄", "24 千歳", "1 札幌", "11 網走", "9 夕張", "18 赤平",
			"8 北見", "30 登別", "27 歌志内", "14 稚内", "3 小樽", "15 美唄", "34 石狩",
			"6 釧路", "17 江別", "35 北斗", "25 滝川", "32 伊達", "12 留萌", "33 北広島",
			"13 苫小牧", "5 室蘭", "28 深川", "4 旭川", "23 根室", "16 芦別", "20 士別",
			"7 帯広", "22 三笠", "26 砂川", "19 紋別", };
	
	final private Person[] MOMOKURO_MEMBERS = {
			new Person("KANAKO Momota", 21, Sex.FEMALE),
			new Person("SHIORI Tamai", 20, Sex.FEMALE),
			new Person("AYAKA Sasaki", 19, Sex.FEMALE),
			new Person("RENI Takagi", 22, Sex.FEMALE),
			new Person("MOMOKA Ariyasu", 20, Sex.FEMALE),
	};

	/**
	 * 素数であるか判定する
	 * 
	 * @param n
	 *            判定させたい数値
	 * @return 素数ならtrue
	 */
	private static boolean isPrime(int n) {
		if (n < 2)
			return false;
		else if (n == 2)
			return true;

		if (n % 2 == 0)
			return false;

		for (int i = 3; i <= n / i; i += 2)
			if (n % i == 0)
				return false;
		return true;
	}

	/**
	 * filterで100までの素数を抽出します. (filter, toArray)
	 */
	@Test
	public void filterTest() {
		int[] result = IntStream.range(1, 100).filter(n -> isPrime(n))
				.toArray();
		assertArrayEquals(PRIMES100, result);
	}

	/**
	 * "別" で終わる地名を抽出します. (filter, toArray)
	 */
	@Test
	public void filterTest2() {
		Stream<String> result = Stream.of(HOKKAIDO_CITIES)
				.filter(s -> s.charAt(s.length() - 1) == '別');
		assertArrayEquals(
				new String[] { "30 登別", "17 江別", "16 芦別", "20 士別", "19 紋別", },
				result.toArray());
	}

	/**
	 * ソートし, 上位3つを抽出する (sorted, limit, toArray)
	 */
	@Test
	public void sortTest() {
		Stream<String> result = Stream.of(HOKKAIDO_CITIES).sorted().limit(3);
		assertArrayEquals(new String[] { "1 札幌", "10 岩見沢", "11 網走", },
				result.toArray());
	}

	/**
	 * 数値としてソートし, 上位3つを抽出する (sorted, map, toArray, reduce)
	 */
	@Test
	public void sortTest2() {
		Stream<String> result = Stream.of(HOKKAIDO_CITIES).sorted((o1, o2) ->
		// これでも同じ. しかし, ちょっとこれは読みづらい…….
		// Integer[] numbers = Stream.of(o1, o2)
		// .map(s -> Integer.parseInt(s.replaceAll("[^0-9]+", "")))
		// .toArray(n -> new Integer[n]);
		// return numbers[0] - numbers[1];
		Stream.of(o1, o2)
				.map(s -> Integer.parseInt(s.replaceAll("[^0-9]+", "")))
				.reduce((m, n) -> m - n).get()).limit(3);
		assertArrayEquals(new String[] { "1 札幌", "2 函館", "3 小樽", },
				result.toArray());
	}

	/**
	 * "川" で終わる地名と, 3文字の地名を抽出します (flatMap, filter, distinct)
	 */
	@Test
	public void flatMapTest() {
		Stream<String> result = Stream
				.of(
						// 末尾の文字 "川" を判定
						(Predicate<String>) s -> s
								.charAt(s.length() - 1) == '川',
						// 3文字であるか判定
						(Predicate<String>) s -> s.split("\\s")[1]
								.length() == 3)
				.flatMap(p -> Stream.of(HOKKAIDO_CITIES).filter(p))
				// 重複を除く
				.distinct();
		assertArrayEquals(
				new String[] { "10 岩見沢", "13 苫小牧", "25 滝川", "26 砂川", "27 歌志内",
						"28 深川", "29 富良野", "33 北広島", "4 旭川", },
				result.sorted().toArray());
	}
	
	/**
	 * count
	 */
	@Test
	public void countTest() {
		assertEquals(Stream.of(HOKKAIDO_CITIES).count(), 35);
	}
	
	/**
	 * Stream API max test
	 */
	@Test
	public void maxTest() {
		assertEquals(9, IntStream.range(1, 10).max().getAsInt());
	}
	
	/**
	 * ももクロメンバーの最年長を調べる
	 */
	@Test
	public void maxTest2() {
		Person p1 = Stream.of(MOMOKURO_MEMBERS).max(
				(o1, o2)-> o1.getAge() - o2.getAge()).get();
		assertEquals("RENI Takagi", p1.getName());
	}
	
	/**
	 * Stream API min test
	 */
	@Test
	public void minTest() {
		assertEquals(1, IntStream.range(1, 10).min().getAsInt());
	}
	
	/**
	 * Stream API sum test
	 */
	@Test
	public void sumTest() {
		assertEquals(45, IntStream.range(1, 10).sum());
	}
}
