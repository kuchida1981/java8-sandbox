/**
 * 
 */
package org.bitbucket.kuchida1981.sandbox.helpers;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

/**
 * standard output interceptor
 * @author kosuke
 *
 */
public class StdoutInterceptor {
	
	/**
	 * internal stream
	 */
	private ByteArrayOutputStream stream;

	/**
	 * this instance
	 */
	private static StdoutInterceptor instance;

	/**
	 * get singleton instance.
	 * @return interceptor
	 */
	public static StdoutInterceptor getInstance() {
		if(instance == null)
			return instance = new StdoutInterceptor();
		else
			return instance;
	}
	
	/**
	 * constructor
	 */
	private StdoutInterceptor() {
		stream = new ByteArrayOutputStream();
	}
	
	/**
	 * begin intercepting stdout.
	 * @return interceptor
	 */
	public StdoutInterceptor begin() {
		System.setOut(new PrintStream(stream));
		return this;
	}
	
	/**
	 * terminate intercepting.
	 */
	public void terminate() {
		System.setOut(new PrintStream(new BufferedOutputStream(
				new FileOutputStream(FileDescriptor.out), 128), true));
	}

	/**
	 * @throws IOException
	 * @see java.io.OutputStream#flush()
	 */
	public void flush() throws IOException {
		stream.flush();
	}

	/**
	 * 
	 * @see java.io.ByteArrayOutputStream#reset()
	 */
	public void reset() {
		stream.reset();
	}

	/**
	 * @return
	 * @see java.io.ByteArrayOutputStream#size()
	 */
	public int size() {
		return stream.size();
	}

	/**
	 * @return
	 * @see java.io.ByteArrayOutputStream#toByteArray()
	 */
	public byte[] toByteArray() {
		return stream.toByteArray();
	}

	/**
	 * @return
	 * @see java.io.ByteArrayOutputStream#toString()
	 */
	public String toString() {
		return stream.toString();
	}

	/**
	 * @param hibyte
	 * @return
	 * @deprecated
	 * @see java.io.ByteArrayOutputStream#toString(int)
	 */
	public String toString(int hibyte) {
		return stream.toString(hibyte);
	}

	/**
	 * @param charsetName
	 * @return
	 * @throws UnsupportedEncodingException
	 * @see java.io.ByteArrayOutputStream#toString(java.lang.String)
	 */
	public String toString(String charsetName) throws UnsupportedEncodingException {
		return stream.toString(charsetName);
	}
}
