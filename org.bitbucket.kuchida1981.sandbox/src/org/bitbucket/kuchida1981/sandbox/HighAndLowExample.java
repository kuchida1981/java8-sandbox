package org.bitbucket.kuchida1981.sandbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HighAndLowExample {

	public static void main(String[] args) {
		int expectedValue = (int) (Math.random() * 10) + 1;
		int enteredCount = 0;
		boolean succeed = false;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			while (++enteredCount <= 10) {
				System.out.println(String.format("数値を入力してください (1-10) (試行 %d 回目)", enteredCount));
				int enteredValue = Integer.parseInt(reader.readLine());
				if (succeed = expectedValue == enteredValue)
					break;
				System.out.println(String.format("%d より %s です.", enteredValue,
						expectedValue > enteredValue ? "大きい" : "小さい"));
			}
			if (succeed)
				System.out.println(String.format("%d 回目で正解しました.", enteredCount));
			else
				System.out.println("10回失敗しました");
		} catch (IOException e) {
			System.out.println("IOエラー");
		} catch (NumberFormatException e) {
			System.out.println("数値を入力してください");
		}
	}
}