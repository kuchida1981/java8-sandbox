package org.bitbucket.kuchida1981.sandbox;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.After;
import org.bitbucket.kuchida1981.sandbox.helpers.StdoutInterceptor;

/**
 * Hello world examples
 * 
 * @author kosuke
 *
 */
public class HelloWorld {

	/**
	 * 差し替え後の標準出力ストリーム
	 */
	private StdoutInterceptor interceptor;

	/**
	 * 期待結果
	 */
	final private String EXPECTED = "Hello world." + System.lineSeparator();

	@BeforeClass
	public void setUpBeforeClass() {
		interceptor = StdoutInterceptor.getInstance();
	}
	/**
	 * 標準出力先を差し替える
	 */
	@Before
	public void setUp() {
		interceptor.begin();
	}

	/**
	 * 標準出力先を元に戻す
	 */
	@After
	public void tearDown() {
		interceptor.terminate();
	}

	@Test
	public void test() {
		System.out.println("Hello world.");
		assertEquals(EXPECTED, interceptor.toString());
	}
}
