/**
 * 
 */
package org.bitbucket.kuchida1981.sandbox.samples;

/**
 * @author kosuke
 * 性別
 */
public enum Sex {
	/**
	 * 男
	 */
	MALE,
	
	/**
	 * 女
	 */
	FEMALE,
	
	/**
	 * 不明
	 */
	UNKNOWN,
}
