/**
 * 
 */
package org.bitbucket.kuchida1981.sandbox.samples;

/**
 * @author kosuke
 * 人間
 */
public class Person {
	
	private String name;
	private int age;
	private Sex sex;
	
	/**
	 * 名前
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 年齢
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * 性別
	 * @return the sex
	 */
	public Sex getSex() {
		return sex;
	}

	/**
	 * コンストラクタ
	 * @param name 名前
	 * @param age 年齢
	 * @param sex 性別
	 */
	public Person(String name, int age, Sex sex) {
		this.name = name;
		this.age = age;
		this.sex = sex;
	}

}
